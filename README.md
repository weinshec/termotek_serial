# termotek_serial

Custom RS2332 command line interface to Termotek P800 series chillers



## Building the package

The code is written in C++ using the ``libserial`` library for
convenient configuration and usage of the serial port. ``libserial``
is included in the package repositories of all major distributions. To
compile the application simply run ``make`` which builds the
executable in the ``installed`` folder.


## Test Suite (Optional)

If you wish to run the test suite you need to install the
``googletest`` C++ unittesting framework and run ``make test``. This
will compile and execute the test suite showing the results on stdout.

