/** \file send.cxx
 * Small test application to test sending and recieving ValueBlocks
 * to/from the chiller device
 *
 * author: Christoph Weinsheimer
 * email:  christoph.weinsheimer@desy.de
 */
#include <iostream>
#include <unistd.h>

#include "termotek.h"
#include "P800.h"



int main(int argc, char* argv[])
{

    if (argc != 2) {
        std::cerr << "Syntax: send <port>" << std::endl;
        return -1;
    }

    std::vector<ValueBlock> blocks;
    blocks.push_back( TVorlaufRohEx );
    // blocks.push_back( TRuecklaufRoh );
    blocks.push_back( DFL1_Fluss    );
    // blocks.push_back( DFL2_Fluss    );
    blocks.push_back( Druck         );
    blocks.push_back( TUmgebung     );
    blocks.push_back( TVorlaufRoh   );
    blocks.push_back( TVorlauf      );
    blocks.push_back( TVorlaufEx    );
    // blocks.push_back( TRuecklauf    );
    blocks.push_back( Pumpe_Fluss   );
    blocks.push_back( GrenzRaumTemp );
    blocks.push_back( TSollwert     );

    RS232 sp(argv[1]);

    for (unsigned int i = 0; i < blocks.size(); i++) {
        sp.send(blocks[i]);
        sp.recv(blocks[i]);

        std::cout << blocks[i].to_string() << std::endl;

        usleep(500);
    }

    return 0;
}
