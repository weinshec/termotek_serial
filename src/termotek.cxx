#include <string>
#include <cstring>
#include <vector>
#include <cmath>
#include <SerialPort.h>


#include "termotek.h"



std::string
helper::hex2literal(unsigned char c)
{
    std::string output = "";
    
    char encoding[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C',
                       'D','E','F'};
    
    output += encoding[ (unsigned char) c >> 4 ];
    output += encoding[ (unsigned char) c & 15 ];
    
    return output;
}



std::string
helper::hex2literal(unsigned char* c, int length)
{
    std::string output = "";
    for (int i = 0; i < length; i++)
        output += hex2literal(c[i]) + " ";

    output.pop_back();

    return output;
}



float
helper::data2float(std::vector<unsigned char> data, unsigned int decimals)
{
    if (data.size() == 0)
        return NaN;

    int hex_val = 0;
    for (unsigned int i = 0; i < data.size(); i++)
        hex_val += (data[i] << 8*i);

    return hex_val / pow(10, decimals);
}



Block::Block(unsigned int len, unsigned char* buf)
{
    std::memcpy(buffer, buf, len*sizeof(unsigned char));
    length = len;
}



unsigned char
Block::calc_checksum(unsigned int length, unsigned char* buffer)
{
    unsigned char sum = 0;
    for (unsigned int i = 0; i < length; i++)
        sum += (unsigned char) buffer[i];
    
    return 0xAA - sum;
}



bool
Block::validateCheckSum()
{
    unsigned char actual = Block::calc_checksum(length-1, buffer);
    unsigned char expect = buffer[length-1];

    return actual == expect;
}



std::string
Block::to_string()
{
    std::string str = "";
    for (unsigned int i = 0; i < length; i++)
        str += helper::hex2literal(buffer[i]) + " ";

    if (str != "")
        str.pop_back();

    return str;
}



std::vector<unsigned char>
Block::DATA()
{
    std::vector<unsigned char> data;
    for (unsigned int i = 0; i < LEN(); i++)
        data.push_back( buffer[i+5] );

    return data;
}



void
Block::setHeaderDataChksum(unsigned char* header,
                           unsigned char* data,
                           unsigned char chksum)
{

    std::memcpy(buffer, header, sizeof(unsigned char)*5);
    std::memcpy(&buffer[5], data, sizeof(unsigned char)*12);
    buffer[5+header[0]] = chksum;

    length = 5 + header[0] + 1;
}



ValueBlock::ValueBlock(std::string   name,
                       unsigned char CMD2,
                       unsigned int  offset,
                       unsigned int  length,
                       unsigned int  decimals)
{

    this->name       = name;
    this->decimals   = decimals;
    Block::length    = 9;
    Block::buffer[0] = 0x03;
    Block::buffer[1] = 0x01;
    Block::buffer[2] = 0x00;
    Block::buffer[3] = 0x13;
    Block::buffer[4] = CMD2;
    Block::buffer[5] = (unsigned char) offset;
    Block::buffer[6] = 0x00;
    Block::buffer[7] = (unsigned char) length;
    Block::buffer[8] = Block::calc_checksum(Block::length-1, Block::buffer);
}



float
ValueBlock::value()
{
    if (SRC() == 0x00)
        return helper::NaN;
    else
        return helper::data2float(DATA(),decimals);
}



std::string
ValueBlock::to_string()
{
    return name + " = " + std::to_string(value());
}



RS232::RS232(std::string pathname)
{
    sp = new SerialPort( pathname );

    try {
        sp->Open( SerialPort::BAUD_38400,
                  SerialPort::CHAR_SIZE_8,
                  SerialPort::PARITY_NONE,
                  SerialPort::STOP_BITS_1,
                  SerialPort::FLOW_CONTROL_NONE );
    } catch (SerialPort::AlreadyOpen &e) {
        std::cout << e.what() << std::endl;
    } catch (SerialPort::OpenFailed &e) {
        std::cout << e.what() << std::endl;
    }
}



RS232::~RS232()
{
    if (sp->IsOpen()) sp->Close();
}



int
RS232::send(Block b)
{
    if (!sp->IsOpen()) {
        std::cerr << "Port is closed. Cannot send transmission block!"
                  << std::endl;
        return -1;
    }

    unsigned char* buf = b.getBuffer();
    SerialPort::DataBuffer buffer(buf, buf + b.getLength() );
    
    sp->Write( buffer );

    if ( sp->ReadByte(200) != 0xAA) {
        std::cerr << "Wrong/No checksum recieved after sending!" << std::endl;
        return -1;
    }

    return 0;
}



int 
RS232::recv(Block &b)
{
    SerialPort::DataBuffer header;

    sp->Read(header, 5, 200);

    unsigned char buffer[240];
    for (unsigned int i = 0; i < header[0]; i++)
        buffer[i] = sp->ReadByte(200);

    unsigned char chksum = sp->ReadByte(200);

    b.setHeaderDataChksum(&header[0], buffer, chksum);

    return 0;
}
