/** \file P800.h
 * Header file defining the communcation blocks specific to the P800 chiller
 * series (values taken from the communication protocol manual)
 *
 * author: Christoph Weinsheimer
 * email:  christoph.weinsheimer@desy.de
 */
#ifndef __P800_H__
#define __P800_H__

#include "termotek.h"


/**
 * IOs
*/
#define TVorlaufRohEx   ValueBlock("TVorlaufRohEx / deg C",  0x01,  36,  2,  2)
#define TRuecklaufRoh   ValueBlock("TRuecklaufRoh / deg C",  0x01,  38,  2,  1)
#define DFL1_Fluss      ValueBlock("DFL1_Fluss / l*min^-1",  0x01,  48,  2,  1)
#define DFL2_Fluss      ValueBlock("DFL2_Fluss / l*min^-1",  0x01,  62,  2,  1)
#define Druck           ValueBlock("Druck / bar",            0x01,  90,  2,  1)


/**
 * Flags
 */
#define TUmgebung     ValueBlock("TUmbegung / deg C",       0x02,  22,   2,  0)
#define TVorlaufRoh   ValueBlock("TVorlaufRoh / deg C",     0x02,  130,  2,  1)
#define TVorlauf      ValueBlock("TVorlauf / deg C",        0x02,  132,  2,  1)
#define TVorlaufEx    ValueBlock("TVorlaufEx / deg C",      0x02,  134,  2,  2)
#define TRuecklauf    ValueBlock("TRuecklauf / deg C",      0x02,  136,  2,  1)
#define Pumpe_Fluss   ValueBlock("Pumpe_Fluss / l*min^-1",  0x02,  152,  2,  1)


/**
 * Parameter
 */
#define GrenzRaumTemp   ValueBlock("Grenzraumtemp / deg C",  0x03,  16,  2,  0)


/**
 * Processdaten
 */
#define TSollwert   ValueBlock("Sollwert / deg C",  0x05,  28,  2,  1)


#define TestBlock   ValueBlock("TestBlock",  0x01,  32,  2,  2)





#endif /* __P800_H__ */
