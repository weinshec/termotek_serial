/** \file termotek.h
 * Main header file for the termotek serial project.
 *
 * author: Christoph Weinsheimer
 * email:  christoph.weinsheimer@desy.de
 */
#ifndef __TERMOTEK_H__
#define __TERMOTEK_H__

#include <iostream>
#include <string>
#include <vector>
#include <SerialPort.h>


/**
 * Namespace for common helper functions.
 * This namespace contains small helper functions that maybe shared throughout
 * the entire project.
 */
namespace helper {

    const float NaN = -9999.9;

    /**
     * Convert a character byte to 2 hex literals.
     * Use this for pretty printing of byte code.
     * @param c character to be converted
     * @return string of length 2 representing the byte character
     */
    std::string hex2literal(unsigned char c);

    /**
     * Convert an arrat of character bytes to a string of 2 hex literals.
     * Use this for pretty printing of byte code.
     * @param c array of characters to be converted
     * @param length length of the character array
     * @return string of length 2 representing the byte character
     */
    std::string hex2literal(unsigned char* c, int length);

    float data2float(std::vector<unsigned char> data, unsigned int decimals);

}





/**
 * Block transmission class.
 * Smallest entity that maybe transmitted or recieved from/to the device
 */
class Block
{
    protected:

        unsigned char  buffer[240];  /**< internal buffer array */
        unsigned int   length = 0 ;  /**< length of the block buffer */


    public:
        /**
         * Constructor.
         * Create empty Block object of length 0 and empty buffer
         */
        Block() {};

        /**
         * Constructor.
         * Create Block object providing the full buffer and its length
         * @param len integer length of the buffer
         * @param buf full block buffer
         */
        Block(unsigned int len, unsigned char* buf);

        /** Destructor */
        virtual ~Block() { };

        /** return LEN field */
        unsigned char  LEN() { return buffer[0]; };

        /** return DST field */
        unsigned char  DST() { return buffer[1]; };

        /** return SRC field */
        unsigned char  SRC() { return buffer[2]; };

        /** return CMD1 field */
        unsigned char  CMD1(){ return buffer[3]; };

        /** return CMD2 field */
        unsigned char  CMD2(){ return buffer[4]; };

        /** return DATA field */
        std::vector<unsigned char> DATA();

        /** return length of internal buffer */
        unsigned int   getLength() { return length; };

        /** return internal buffer */
        unsigned char* getBuffer() { return buffer; };

        /**
         * Validate the checksum field.
         * Calculate the internal buffers checksum and return true if it
         * matches the last buffer field.
         * @return true on valid checksum
         */
        bool validateCheckSum();

        /**
         * String representation.
         * Returns a string representation in hex literals.
         * @return string of hex literals
         */
        std::string to_string();

        /**
         * Set the Blocks length and buffer given header, data and checksum.
         * Set the Blocks length and buffer given the header (5 byte), data and
         * checksum byte. This is is useful when reading the answer block from
         * the device.
         * @param header   5-byte header array
         * @param data     array with length corresponding to header[0]
         * @param checksum the checksum byte
         */
        void setHeaderDataChksum(unsigned char* header,
                                 unsigned char* data,
                                 unsigned char chksum);

        /**
         * Calculate the checksum of some buffer.
         * Returns the checksum of a given buffer over the provided length.
         * @param length integer length of the buffer
         * @param buffer checksum of the buffer with given length
         */
        static unsigned char calc_checksum(unsigned int   length,
                                           unsigned char* buffer);

};





/**
 * ValueBlock inheriting from Block representing device data exchange entities
 */
class ValueBlock : public Block
{
    protected:
        std::string name;        /**< Value name this block represents */
        unsigned int decimals;   /**< number of decimal places (see manual) */

    public:
        /**
         * Constructor.
         * Create a value Block instance
         * @param name value name this block represents
         * @param CMD2 hex byte specifying the parameter group (0x01 = IOs,...)
         * @param offset integer value of location inside the parameter group
         * @param length integer value of byte-length of this value
         * @param decimals number of decimal places
         */
        ValueBlock(std::string   name,
                   unsigned char CMD2,
                   unsigned int  offset,
                   unsigned int  length,
                   unsigned int  decimals);

        /** return numerical value as float */
        float value();

        /** return string representation */
        std::string to_string();
};





/**
 * RS232 communication class building a convenient interface to the
 * serial RS232 port
 */
class RS232
{

    SerialPort* sp;  /**< Pointer to the internal serialport instance */

    public:
        /**
         * Constructor.
         * Create a RS232 instance given the file descriptor path
         * @param pathname unix path to the serial port in use
         */
        RS232(std::string pathname = "/dev/ttyS0");

        /** Destructor */
        ~RS232();

        /**
        * Send a block to device.
        * @param b block instance to send
        */
        int send(Block b );
  
        /**
        * Recieve a block from device.
        * @param b block instance to store the recieved one in
        */
        int recv(Block &b);
};

#endif /* __TERMOTEK_H__ */
