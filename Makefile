################################################################################
#                                                                           Init
################################################################################

LIBS    = -lserial
INCLUDE = 

CXXFLAGS = -ansi -Wall -Wno-long-long -pedantic -pedantic-errors -fPIC -std=c++11


################################################################################
#                                                                        Project
################################################################################

SRC  = ${PWD}/src
INC  = ${PWD}/include
INST = ${PWD}/installed
TEST = ${PWD}/test
DOC  = ${PWD}/doc


OBJ  = ${INST}/termotek.o

DEPS = ${OBJ:.o=.d}


all: ${INST}/send


test: ${TEST}/tests
	@echo ""
	@echo "Running test suite"
	@echo ""
	${TEST}/tests


.PHONY: doc
doc:
	@echo ""
	@echo "Creating documentation"
	@echo ""
	cd ${DOC}; doxygen doxy.conf; make html;


-include ${DEPS}

${INST}/%.d: ${SRC}/%.cxx
	@$(CXX) -MM -MT "$@ $(patsubst %.d,%.o,$@)" -MF $@ -I${INC} $<


${INST}/%.o: ${SRC}/%.cxx
	@echo ""
	@echo "Building $*"
	@echo ""
	$(CXX) -c $< ${LIBS} ${INCLUDE} -I${INC} ${CXXFLAGS} -o $@


${INST}/send: ${SRC}/send.cxx ${OBJ}
	@echo ""
	@echo "Building send"
	@echo ""
	$(CXX) $< ${OBJ} ${LIBS} ${INCLUDE} -I${INC} ${CXXFLAGS} -o $@


${TEST}/tests: ${TEST}/tests.cxx ${OBJ}
	@echo ""
	@echo "Building test suite"
	@echo ""
	$(CXX) $< -lgtest ${OBJ} ${LIBS} ${INCLUDE} -I${INC} ${CXXFLAGS} -o $@


.PHONY: clean
clean:	
	@echo clean
	@if [ -f ${TEST}/tests ] ; then rm ${TEST}/tests ; fi ;
	@rm -rf ${INST}/*

