/**
 * tests.cxx
 *
 * Run unittests based on the googletest unit testing framework
 *
 * author: Christoph Weinsheimer
 * email:  weinshec@students.uni-mainz.de
 */

#include <vector>
#include <gtest/gtest.h>

#include "termotek.h"
#include "P800.h"


namespace
{

    class BlockTests : public ::testing::Test
    {
        protected:

            unsigned int  blength;
            unsigned char buffer[9];

            virtual void SetUp()
            {
                /* Example from p.15 upper half */
                blength = 9;
                buffer[0] = 0x03; // LEN
                buffer[1] = 0x01; // DST
                buffer[2] = 0x00; // SRC
                buffer[3] = 0x13; // CMD1
                buffer[4] = 0x01; // CMD2
                buffer[5] = 0x20; // Data
                buffer[6] = 0x00; // ...
                buffer[7] = 0x02; // ...
                buffer[8] = 0x70; // CHKSUM
            };
    };

}



TEST(HelperTests, TestHex2LiteralSingleChar)
{
    unsigned char c = 0xA4;
    ASSERT_EQ("A4", helper::hex2literal(c));
}



TEST(HelperTests, TestHex2LiteralFullString)
{
    unsigned char c[5] = {0xA4, 0xFF, 0x12, 0x00, 0x4C};
    ASSERT_EQ("A4 FF 12 00 4C", helper::hex2literal(c,5));
}



TEST(HelperTests, TestData2FloatLength2)
{
    std::vector<unsigned char> data;
    data.push_back(0x30); 
    data.push_back(0x06);

    ASSERT_FLOAT_EQ(15.84, helper::data2float(data,2));
}



TEST(HelperTests, TestData2FloatDecimals)
{
    std::vector<unsigned char> data;
    data.push_back(0x30); 
    data.push_back(0x06);

    ASSERT_FLOAT_EQ(1584., helper::data2float(data,0));
    ASSERT_FLOAT_EQ(0.1584, helper::data2float(data,4));
}



TEST(HelperTests, TestData2FloatOtherLength)
{
    std::vector<unsigned char> data;
    ASSERT_FLOAT_EQ(helper::NaN, helper::data2float(data,2));


    data.push_back(0x30); 
    ASSERT_FLOAT_EQ(0.48, helper::data2float(data,2));

    data.push_back(0x06);
    data.push_back(0x06);
    ASSERT_FLOAT_EQ(3948.0, helper::data2float(data,2));
}



TEST_F(BlockTests, TestCheckSumCalculation)
{
    unsigned char buf[] = {0x03, 0x01, 0x00, 0x13, 0x01, 0x20, 0x00, 0x02};
    ASSERT_EQ(0x70, Block::calc_checksum(8, buf));
}



TEST_F(BlockTests, TestValidateChecksum)
{
    Block block(blength, buffer);
    ASSERT_TRUE(block.validateCheckSum());
}



TEST_F(BlockTests, TestParsesMembersCorrectly)
{
    Block b(blength, buffer);
    ASSERT_EQ(0x03, b.LEN());
    ASSERT_EQ(0x01, b.DST());
    ASSERT_EQ(0x00, b.SRC());
    ASSERT_EQ(0x13, b.CMD1());
    ASSERT_EQ(0x01, b.CMD2());
    ASSERT_TRUE( b.validateCheckSum() );
}



TEST_F(BlockTests, TestSetHeaderDataChksum)
{
    unsigned char header[5] = {0x04, 0x01, 0x00, 0x15, 0x00};
    unsigned char data[4]   = {0x01, 0x02, 0x00, 0x01};
    unsigned char chksum    = 0x8C;

    Block b(blength, buffer);
    b.setHeaderDataChksum(header, data, chksum);

    ASSERT_EQ(0x04, b.LEN());
    ASSERT_EQ(0x01, b.DST());
    ASSERT_EQ(0x00, b.SRC());
    ASSERT_EQ(0x15, b.CMD1());
    ASSERT_EQ(0x00, b.CMD2());

    ASSERT_TRUE( b.validateCheckSum() );
}



TEST_F(BlockTests, TestToString)
{
    Block b(blength, buffer);
    std::string exp("03 01 00 13 01 20 00 02 70");
    ASSERT_EQ(exp, b.to_string());
}



TEST_F(BlockTests, TestGetDataBlockAsVector)
{
    Block b(blength, buffer);

    std::vector<unsigned char> expected;
    expected.push_back( 0x20 );
    expected.push_back( 0x00 );
    expected.push_back( 0x02 );

    std::vector<unsigned char> data = b.DATA();

    ASSERT_EQ(expected.size(), data.size()) << "Vector not equally lengthed";
    for (unsigned int i = 0; i < b.LEN(); i++)
        ASSERT_EQ(expected[i], data[i]) << "Vectors differ at index " << i;
}



TEST(ValueBlockTests, TestConstructor)
{
    ValueBlock sollwert("Sollwert", 0x01, 28, 2, 2);

    ASSERT_EQ  ( 9   , sollwert.getLength()  ) << "length missmatch";
    ASSERT_EQ  ( 0x03, sollwert.LEN()        ) << "LEN missmatch";
    ASSERT_EQ  ( 0x01, sollwert.DST()        ) << "DST missmatch";
    ASSERT_EQ  ( 0x00, sollwert.SRC()        ) << "SRC missmatch";
    ASSERT_EQ  ( 0x13, sollwert.CMD1()       ) << "CMD1 missmatch";
    ASSERT_EQ  ( 0x01, sollwert.CMD2()       ) << "CMD2 missmatch";
    ASSERT_EQ  ( 0x1C, sollwert.DATA()[0]    ) << "DATA[0] missmatch";
    ASSERT_EQ  ( 0x00, sollwert.DATA()[1]    ) << "DATA[1] missmatch";
    ASSERT_EQ  ( 0x02, sollwert.DATA()[2]    ) << "DATA[2] missmatch";
    ASSERT_TRUE( sollwert.validateCheckSum() ) << "Checksum missmatch";
}



TEST(ValueBlockTests, TestReturnValueParsing)
{
    ValueBlock sollwert("Sollwert", 0x01, 28, 2, 2);
    ASSERT_FLOAT_EQ( helper::NaN, sollwert.value() );

    unsigned char answerHeader[] = {0x02, 0x00, 0x01, 0x13, 0x01};
    unsigned char answerData[]   = {0x30, 0x06};
    unsigned char answerChkSum   = 0x5D;
    sollwert.setHeaderDataChksum(answerHeader, answerData, answerChkSum);
    ASSERT_FLOAT_EQ( 15.84, sollwert.value() );
}





int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
